/*
 * @Date: 2020-05-20 10:15:13
 * @Author: 	LingMax[xiaohxx@qq.com]
 * @最后编辑: 	LingMax[xiaohxx@qq.com]
 * @文件详情: 	文件详情
 */
var PY = require('pinyin-match');
var fs = require('fs');
var pinyin = require("chinese-to-pinyin");

var book =  "C:/Users/Administrator/AppData/Local/360Chrome/Chrome/User Data/Default/360UID567242567_V8/Bookmarks";
var book_json = {}
try {
  book_json = JSON.parse(fs.readFileSync(book));
} catch (error) {}

//1.5秒内只能触发一次
var sst = 0;
function search(val, call) {
    var arg = arguments;
    search2.apply(this,arg);
    return;
    sst++
    setTimeout(() => {
        sst--
        if(sst == 0)search2.apply(this,arg);
    }, 800);
}

/**
 * @function: 	搜索
 * @author: 	LingMax[xiaohxx@qq.com]
 * @params: 	Object	val		文本 空格分隔文字
 * @return: 	Array|String|int|Boolean|Object|*
 */
function search2(val, call) {
    var vs = val.replace(/\s+/g, ' ').split(' ');//空格拆分关键字
    var ss = [];//结果
    var ssObj = {
        'name': '',
        'dec': '',
        'ico': '',
        'fun': '',
    };
    var reg  = [];
    for(var i = 0; i < vs.length; i++) {
        //pinyin(vs[i],{keepRest: true ,removeTone: true});
        reg[i] = (new RegExp(vs[i].replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"), 'img'));
    }


    new Promise((r) => {
        r('成功') // 数据处理完成
    }).then((res) => {//软件
        return new Promise((r) => {
            db.all('select * from obj order by sortx desc', [], function (err, rows) {
                var ls, bz = false, d, ls1;//查询数据匹配
                for (const v of rows) {
                    ls1 = v.recommend.replace(/<(style|script|iframe)[^>]*?>[\s\S]+?<\/\1\s*>/gi, '').replace(/<[^>]+?>/g, '').replace(/\s+/g, ' ').replace(/ /g, ' ').replace(/>/g, ' ');
                    ls = v.name + ' ' + ls1;
                    bz = false;//匹配关键字 
                    for (let i = 0; i < vs.length; i++) {
                        if (vs[i] == '' || !(reg[i].test(ls) || PY.match(ls, vs[i]) )) {
                            bz = true;
                            break;
                        }
                    }
                    if (!bz) {
                        d = Object.assign({}, ssObj);
                        d.name = v.name;
                        d.dec = ls1;
                        d.ico=v.ico?v.ico:'/img/rj.png';
                        // d.ico = 'data/userData/img/' + cryptoX.createHash('sha256').update(v.path).digest('hex') + '_LingMax.png';
                        
                        // if(!fs.existsSync(d.ico)){
                        //     d.ico = '/img/rj.png';
                        // }else{
                        //     d.ico='/'+d.ico; 
                        // }
                        
                        d.fun=`var ls='html/tools.html#`+v.classx+`',ys = document.querySelector('a[data-url="'+ls+'"]');
                        $('.layui-nav-itemed').removeClass('layui-nav-itemed');
                        $(ys.parentNode.parentNode.parentNode).addClass('layui-nav-itemed');ys.click()
                        var sjs=Math.random(),
                        lj1 = ()=>{
                            var ccwd = \`(()=>{
                                var cbb = '\`+sjs+\`';
                                var as = document.querySelector('button[data-path=\\"`+v.id+`\\"]');
                                 if(window.runLog!==cbb && as){
                                    window.runLog=cbb;
                                    as=$(as); 
                                    as.parents('.layui-colla-content').removeClass('layui-show').addClass('layui-show');
                                    as[0].click();
                                }
                            })()\`
                            win.eval(document.querySelector('[src="'+ls+'"]'), ccwd);
                        }
                        ;
                        setTimeout(lj1,50);
                        setTimeout(lj1,300);
                        setTimeout(lj1,800);
                        setTimeout(lj1,1500);
                        setTimeout(lj1,3000);
                        `;
                        ss.push(d);
                    }
                }
                r('成功') // 数据处理完成
            });
        })
    }, (err) => { }).then((res) => {//小脚本
        return new Promise((r) => {
            db.all('select * from exec order by sortx desc', [], function (err, rows) {
                var ls, bz = false, d, ls1;//查询数据匹配
                for (const v of rows) {
                    ls1 =  v.info;
                    ls = v.name + ' ' + ls1;
                    bz = 0;//匹配关键字 
                    for (let i = 0; i < vs.length; i++) {
                        if (vs[i] == '' || reg[i].test(ls) || PY.match(ls, vs[i]) ) {
                            bz++;
                            break;
                        }
                    }
                    if (bz == vs.length) {
                        d = Object.assign({}, ssObj);
                        d.name = v.name;
                        d.dec = ls1;
                        d.ico = '/img/dm.png';
                        d.fun='if(!confirm("确定运行命令吗？")){trwi.gffg.fggf()}var d = '+JSON.stringify(v)+';runAllExeC(d);';
                        ss.push(d);
                    }
                }
                r('成功') // 数据处理完成
            });
        });
    }, (err) => { }).then((res) => {//POST接口
        return new Promise((r) => {
            db.all('select * from post', [], function (err, rows) {
                var ls, bz = false, d, ls1;//查询数据匹配
                for (const v of rows) {
                    ls1 ='';
                    ls = v.name;
                    bz = 0;//匹配关键字 
                    for (let i = 0; i < vs.length; i++) {
                        if (vs[i] == '' || reg[i].test(ls) || PY.match(ls, vs[i]) ) {
                            bz++;
                            break;
                        }
                    }
                    if (bz == vs.length) {
                        d = Object.assign({}, ssObj);
                        d.name = v.name;
                        d.dec = ls1;
                        d.ico = '/img/post.png';
                        d.fun=`var ls='html/POST.html',ys = document.querySelector('a[data-url="'+ls+'"]');
                        $('.layui-nav-itemed').removeClass('layui-nav-itemed');
                        $(ys.parentNode.parentNode.parentNode).addClass('layui-nav-itemed');ys.click()
                        var sjs=Math.random(),
                        lj1 = ()=>{
                            var ccwd = "(()=>{var cbb = '"+sjs+"';var as;as= $('a[data-id=\\"`+v.id+`\\"]'); if(window.runLog!==cbb && as.length>0){window.runLog=cbb;as.parents('.ddab').removeClass('layui-nav-itemed').addClass('layui-nav-itemed');as.click();}})()"
                            win.eval(document.querySelector('[src="'+ls+'"]'), ccwd);
                        }
                        ;
                        setTimeout(lj1,1500);
                        `;
                        ss.push(d);
                    }
                }
                r('成功') // 数据处理完成
            });
        })
    }, (err) => { }).then((res) => {//网页
        return new Promise((r) => {
            //process.env.LOCALAPPDATA
            console.log(222,ss)
            function json_desc(o) {
                for (var k in o) {
                    if (!o.hasOwnProperty(k) || typeof o[k] != "object") continue;
                    if(o[k].children instanceof Array){
                        for(var i2 = 0; i2 < o[k].children.length; i2++) {
                            var v= o[k].children[i2];
                            if(v.url) {
                                var ls = v.name +v.url
                                bz = 0;//匹配关键字 
                                console.log(ls);
                                for (var i = 0; i < vs.length; i++) {
                                    if (vs[i] == '' || reg[i].test(ls) || PY.match(ls, vs[i]) ) {
                                        bz++;
                                        break;
                                    }
                                }
                                if (bz == vs.length) {
                                    ss.push({
                                        name:v.name,
                                        dec:v.url,
                                        ico:'/img/wy.png',
                                        fun:'nw.Shell.openExternal("'+v.url+'");',
                                    });
                                }
                            }
                        }
                    }
                    json_desc(o[k]);
                }
                
            }
            json_desc(book_json);
            console.log(111,ss)
            r('成功'); // 数据处理完成
            return;

            db.all('select * from web order by sortx desc', [], function (err, rows) {
                var ls, bz = false, d, ls1;//查询数据匹配
                for (const v of rows) {
                    ls1 =  v.info+v.addr;
                    ls = v.name + ' ' + ls1;
                    bz = false;//匹配关键字 
                    for (var i = 0; i < vs.length; i++) {
                        if (vs[i] == '' || !((new RegExp(vs[i].replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"), 'img')).test(ls) || PY.match(ls, vs[i]) )) {
                            bz = true;
                            break;
                        }
                    }
                    if (!bz) {
                        d = Object.assign({}, ssObj);
                        d.name = v.name;
                        d.dec = ls1;
                        d.ico =  v.ico?v.ico:'/img/wy.png';
                        d.fun='nw.Shell.openExternal("'+v.addr+'");';
                        ss.push(d);
                    }
                }
                r('成功') // 数据处理完成
            });
        });
    }, (err) => { }).then((res) => {//完成回调
        //console.log(ss);
        call(ss);
        //r('成功') // 数据处理完成
    }, (err) => { })
}
